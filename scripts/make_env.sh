#!/usr/bin/env bash

# parameters
build_dir=build
python_exe=python3



# create
mkdir -p ${build_dir}
virtualenv -p ${python_exe} ${build_dir}

# activate
source ${build_dir}/bin/activate

# setup
pip install -r requirements.txt
pip install -e .

# cleanup
deactivate
echo "run '${build_dir}/bin/activate' to get into env and 'deactivate' to close it."
