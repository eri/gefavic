#!/usr/bin/python # 3
# -*- coding: utf-8 -*-

from re import sub, finditer

from setuptools import setup
from setuptools import find_packages


def find_description():
    with open("README.md") as file:
        return file.read()


def find_requirements():
    with open("requirements.txt") as file:
        return [line.strip() for line in file.readlines()]


def find_meta_data():
    with open("gefavic/__init__.py") as file:
        return {
            sub(r'^title$', "name", match.group(1)): match.group(2)
            for match in finditer(r'__([^\n]+)__\s*=\s*"([^\n]+)"', file.read())
        }


setup(
    long_description=find_description(),
    packages=find_packages(),
    install_requires=find_requirements(),
    entry_points={"console_scripts": ["gefavic = gefavic.cli:main"]},
    classifiers=[
        "Environment :: Console",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Natural Language :: English",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
    ],
    **find_meta_data()
)


