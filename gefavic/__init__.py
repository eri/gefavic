#!/usr/bin/env python # 3
# -*- coding: utf-8 -*-


from gefavic.getfavicon import main as getfavicon


__title__ = "gefavic"
__version__ = "0.0.1"
__author__ = "eri!"
__author_email__ = "eri@c3d2.de"
__license__ = "LGPL3"
__copyright__ = "Copyright 2020 me"
__description__ = "favicon requester"
__url__ = "https://gitea.c3d2.de/eri/gefavic"




def run(options):
    getfavicon(options)

