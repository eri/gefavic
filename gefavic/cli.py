#!/usr/bin/env python # 3
# -*- coding: utf-8 -*-

import sys
from argparse import ArgumentParser

import gefavic


### Arguments

def add_global_arguments(parser):
    parser.add_argument(
        "--version",
        action='version',
        version=gefavic.__version__,
        help="Show tool version."
    )
    parser.add_argument(
        "url",
        metavar="URL",
        nargs="?",
        help="URL from where the favicon will be requested."
    )
    return parser






### Main

def global_parser():
    parser = add_global_arguments(ArgumentParser(
        description="favicon requester",
        add_help=True,
    ))
    return parser

def main():
    parser = global_parser()
    (options, args) = parser.parse_known_args(sys.argv[1:])
    #print(options, args)
    kwargs = vars(options)
    gefavic.run(options)
    sys.exit(0)

